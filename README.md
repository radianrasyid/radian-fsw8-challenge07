# Challenge 07
Project challenge 7 binar Muhammad Radian Rasyid FSW-8. Project ini memiliki output untuk membuat sebuah project yang berisikan implementasi service repository. Pada project ini tidak diimplementasikan frontend dalam bentuk apapun, sehingga untuk menjalankan project ini harus menggunakan postman dan openAPI sebagai dokumentasi Rest API yang merepresentasikan penggunaan API nantinya.

# PLEASE READ THIS
Project ini menggunakan server dari project challenge 06 di link berikut [CHALLENGE 06](https://gitlab.com/radianrasyid/radian-fsw8-challenge06). Untuk bisa memulai server, bisa menjalankan perintah __npm install__ lalu melakukan setup database dan melakukan seeding data. Setelah itu bisa menjalankan server dengan menggunakan perintah __npm start__ pada terminal.

### Techs
Untuk bisa memulai project ini setelah melakukan langkah setup server dari project challenge 06, bisa dilakukan __npm install__ untuk menginstall semua dependencies yang ada di dalam project ini. Beberapa dependencies yang digunakan pada project ini adalah redux, flickity, bootstrap, jquery, dan react-router-dom. 

### IMPORTANT
Project ini merupakan project react yang pada dasarnya menggunakan port 3000. Oleh karena itu pada project challenge 6 atau server saya memberikan port baru yaitu 3001. Jika nanti data tidak bisa dibaca atau gagal untuk didapatkan dari server, bisa dilihat kembali port yang digunakan pada project ini atau pada project challenge 6. Fetching API dari project ini ke project challenge 6 terdapat pada folder src/features yang mana berisi setup redux yang digunakan pada challenge ini.

### RUNNING STEPS
 - Untuk bisa melakukan hit API, bisa menggunakan link __http://localhost:3001/api/cars__ , namun jika sewaktu-waktu terjadi perubahan port maka nanti bisa disesuaikan.
 - Lakukan __npm install__ untuk menginstall semua dependencies yang ada dan gunakan perintah __npm start__ untuk running project.
 - Link untuk memlihat hasil fetching data dari API dan penggunaan redux bisa dilihat pada link __http://localhost:3000/sewa__
 - Data yang akan ditampilkan pada saat awal membuka halaman adalah kosong, sehingga harus dicari terlebih dahulu.
 - Data mobil bisa dicari pada __23 MARET 2022__, selebihnya bisa menggunakan preferensi pengguna.
