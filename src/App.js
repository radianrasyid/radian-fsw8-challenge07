import { Routes, Route } from 'react-router-dom';
import './App.css';
import './Components/Styles/Global.css'
import './Components/Styles/flickity.css'
import Sewa from './Routes/Sewa';
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap";
import Home from './Routes/Home';

function App() {
  return (
    <>
      <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossOrigin="anonymous"/>
      <link rel="stylesheet" href="https://unpkg.com/flickity@2/dist/flickity.min.css"/>
      
      <Routes>
        <Route path='/sewa' element={<Sewa/>} />
        <Route path='/' element={<Home/>}/>
      </Routes>
    </>
  );
}

export default App;
