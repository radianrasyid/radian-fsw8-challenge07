import React, {useEffect, useState} from 'react'
import './Styles/Global.css'
import carImage from '../Assets/images/car.svg'
import carBg from '../Assets/images/bg.svg'
import 'jquery-ui-dist/jquery-ui';
import pictCalendar from './images/fi_calendar.svg'
import pictSetting from './images/fi_settings.svg'
import pictUser from './images/fi_users.svg'
import { useSelector, useDispatch } from 'react-redux';
import { getAllCars, fetchCars } from '../features/counter/counterSlice';

export default function Body() {
  const [carsFilter, setCarsFilter] = useState([])
  const [capacity, setCapacity] = useState()
  const [waktu, setWaktu] = useState()
  const [tanggal, setTanggal] = useState()
  const [driver, setDriver] = useState()


  const dispatch = useDispatch();
  const cars = useSelector(getAllCars);
  const carsStatus = useSelector((state) => state.cars.status);
  const error = useSelector((state) => state.cars.error);

  

  const filterCars = () =>{
    const filteredCar =  cars.filter(item => {
     let datetime = new Date(tanggal + " " + waktu)
     let date = new Date(item.available);
     let newDate = date.getTime()
 
     const beforeEpochTime = datetime.getTime()
     let filterTanggal = newDate > beforeEpochTime;
     let filterCapacity = item.capacity <= capacity;
     let filterDriver = item.driver === driver;
     return filterCapacity && filterTanggal && filterDriver;
    })
    console.log(filteredCar);
    setCarsFilter(filteredCar);
   }

  useEffect(() => {
    if (carsStatus === 'idle') {
      dispatch(fetchCars());
    }
    console.log("CarsStatus", carsStatus);
  }, [carsStatus, dispatch]);

  let content;
  if (carsStatus === 'loading') {
    content = <div>Loading...</div>;
  } else if (carsStatus === 'succeeded') {
    content = (
        <>
        <section className="home">
          <div className="row" style={{ width: '100%' }}>
            <div className="col-lg-6">
                <div className="container-fluid container-home-greet" style={{ paddingTop: '100px' }}>
                  <h1 className="greet">
                    Sewa & Rental Mobil Terbaik di kawasan Batam
                  </h1>
                  <p className="greet-desc" style={{ fontSize: '1rem' }}>
                    Selamat datang di Rad Car Rental. Kami menyediakan mobil kualitas terbaik dengan harga terjangkau. Selalu siap melayani kebutuhanmu untuk sewa mobil selama 24 jam.
                  </p>
                </div>
            </div>
            <div className="col-lg-6 wrapper-car" style={{ alignItems: 'flex-end' }}>
                  <img className="big-img" src={carBg} alt="" />
                  <img className="small-img" src={carImage} alt="" />
              </div>
          </div>
        </section>
    
          <div className="container container-pilih">
            <div className="row row-sewa p-3">
              <div className="col-lg-10">
                <div className="row">
                  <div className="col-lg-3">
                    <label className="form-label" htmlFor="driver">Tipe Driver</label>
                    <select className="form-select" id="driver" onInput={(e) => setDriver(Number(e.target.value))}>
                      <option selected>Silahkan pilih tipe driver</option>
                      <option value="0">Dengan Sopir</option>
                      <option value="1">Tanpa Sopir (Lepas Kunci)</option>
                    </select>
                  </div>
                  <div className="col-lg-3">
                    <label htmlFor="tanggal" className="form-label">Tanggal</label>
                    <input type="date" className="form-control" id="tanggal" onChange={(e) => setTanggal((e.target.value))}/>
                  </div>
                  <div className="col-lg-3">
                    <label className="form-label" htmlFor="waktu">Waktu Jemput/Ambil</label>
                    <input type="time" id="waktu" className="form-control" placeholder="00:00" onChange={(e) => setWaktu((e.target.value))} />
                  </div>
                  <div className="col-lg-3">
                      <label htmlFor="penumpang" className="form-label">Jumlah Penumpang</label>
                    <input id="penumpang" type="number" className="form-control" placeholder="Jumlah Penumpang" onChange={(e) => setCapacity(Number(e.target.value))} />
                  </div>
                </div>
              </div>
              <div className="col-lg-2">
                <div className="row">
                  <div className="col-lg">
                    <button id="load-btn" className="btn btn-success text-light" style={{ height: '36px', marginTop: '2.1rem' }} onClick={filterCars}>Cari Mobil</button>
                  </div>
                </div>
              </div>
           </div>
          </div>
        <div className='container'>
        <div id="cars-container" className='row'>
            {carsFilter.map(
              item => {
                return(
                  <div key={item.id} className='col-lg-4 col-md-12 col-sm-12'>
                    <div className="card" style={{padding: '20px'}}>
                  <img src={item.image} alt="" className="card-img-top" style={{ height:'400px', objectFit: 'cover' }}/>
                    <div className="card-body w-100">
                      <p className="card-title" style={{ fontWeight: '400' }}>{item.model}</p>
                      <p style={{ fontWeight:'700' }}>Rp{item.rent.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')} / hari</p>
                      <p>{item.description.slice(0, 38)}</p>
                      <p><img src={pictUser} alt="" style={{ width:'16px' }}/> {item.capacity} orang</p>
                      <p><img src={pictSetting} alt="" style={{ width:'16px' }}/> {item.transmission}</p>
                      <p><img src={pictCalendar} alt="" style={{ width:'16px' }}/> Tahun {item.year}</p>
                      <p>{item.available}</p>
                      <button id="cari" type="button" className="btn btn-success text-light w-100">Pilih Mobil</button>
                    </div>
                  </div>
                  </div>
                )
              }
            )
            }
        </div>
        </div>
        </>
    );
  } else if (carsStatus === 'failed') {
    content = (
      <div>{error}</div>
    )
  }

  return <>{content}</>
}
