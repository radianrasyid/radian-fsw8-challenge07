import React from 'react'
import 'flickity'

export default function HomeBody() {
  return (
    <>
        <section className="home">
        <div className="row" style={{ width: '100%' }}>
            <div data-aos="fade-up" data-aos-duration="1000" data-aos-mirror="true" className="col-lg-6">
                <div className="container-fluid container-home-greet" style={{ paddingTop: '100px' }}>
                <h1 className="greet">
                    Sewa & Rental Mobil Terbaik di kawasan Batam
                </h1>
                <p className="greet-desc">
                    Selamat datang di Rad Car Rental. Kami menyediakan mobil kualitas terbaik dengan harga terjangkau. Selalu siap melayani kebutuhanmu untuk sewa mobil selama 24 jam.
                </p>
                <a href="#sewa" className="button-sewa text-decoration-none">Mulai Sewa Mobil</a>
                </div>
            </div>
            <div className="col-lg-6 wrapper-car" style={{ alignItems: 'flex-end' }}>
                <img className=" big-img" src="images/bg.svg" alt=""/>
                <img className=" small-img" src="images/car.svg" alt=""/>
            </div>
        </div>
        </section>

        
        {/* OUR SERVICES */}
        <div className="container-fluid container-sect">
        <div id="our-service-greet" className="row">
            <div data-aos="zoom-in" className="col-lg-6">
            <div className="col-lg-12 mt-5">
                <div className='wrapper-girl'>
                <img src="images/Our_Service/Ellipse_21.svg" alt="" className="img-responsive big-img-girl"/>
                <img src="images/Our_Service/happy_girl.svg" alt="" className="img-responsive small-img-girl"/>
                <img src="images/Our_Service/Ellipse_19.svg" alt="" className="img-responsive small-img-circ1"/>
                <img src="images/Our_Service/Ellipse_20.svg" alt="" className="img-responsive small-img-circ2"/>
                <img src="images/Our_Service/Ellipse_22.svg" alt="" className="img-responsive small-img-circ3"/>
                </div>
            </div>
            </div>
            <div className="col-lg-6 mt-5">
            <div className="container">
                <h1 className="our-service-greet" style={{ marginBottom: '24px' }}>Best Car Rental for any kind of trip in Batam!</h1>
                <p className="our-service-desc" style={{ marginBottom: '18px' }}>Sewa mobil di Batam bersama Rad Car Rental jaminan harga lebih murah dibandingkan yang lain, kondisi mobil baru, serta kualitas pelayanan terbaik untuk perjalanan wisata, bisnis, wedding, meeting, dll.</p>
                <ul>
                <li data-aos="fade-up"><p><img src="images/Our_Service/Group_53.svg" alt="" style={{ marginRight: '16px' }}/>Sewa Mobil Dengan Supir di Batam 12 Jam</p></li>
                <li data-aos="fade-up"><p><img src="images/Our_Service/Group_53.svg" alt="" style={{ marginRight: '16px' }}/>Sewa Mobil Lepas Kunci di Batam 24 Jam</p></li>
                <li data-aos="fade-up"><p><img src="images/Our_Service/Group_53.svg" alt="" style={{ marginRight: '16px' }}/>Sewa Mobil Jangka Panjang Bulanan</p></li>
                <li data-aos="fade-up"><p><img src="images/Our_Service/Group_53.svg" alt="" style={{ marginRight: '16px' }}/>Gratis Antar - Jemput Mobil di Bandara</p></li>
                <li data-aos="fade-up"><p><img src="images/Our_Service/Group_53.svg" alt="" style={{ marginRight: '16px' }}/>Layanan Airport Transfer / Drop In Out</p></li>
                </ul>
            </div>
            </div>
        </div>
        </div>

        {/* WHY US */}
        <p id="whyus"></p>
        <div className="container-fluid container-sect">
        <div className="container">
            <h1 className="whyus">Why Us</h1>
            <p className="whyus-desc">Mengapa harus pilih Rad Car Rental?</p>
        </div>
        <div className="container">
            <div className="row">
                <div className="col-lg" data-aos="fade-right">
                        <div className="card w-100" style={{ width: 'fit-content' }}>
                            <div className="card-body">
                            <img src="images/why_us/icon_complete.svg" className="img-responsive mb-3" width="32" alt=""/>
                            <h5 className="card-title">Mobil Legkap</h5>
                            <p className="card-text">Tersedia banyak pilihan mobil, kondisi masih baru, bersih dan terawat</p>
                            </div>
                        </div>
                </div>
                <div className="col-lg" data-aos="fade-right" data-aos-delay="50">
                    <div className="card w-100" style={{ width: 'fit-content' }}>
                        <div className="card-body">
                            <img src="images/why_us/icon_price.svg" className="img-responsive mb-3" width="32" alt=""/>
                            <h5 className="card-title">Harga Murah</h5>
                            <p className="card-text">Harga murah dan bersaing, bisa bandingkan harga kami dengan rental mobil lain</p>
                        </div>
                    </div>
                </div>
                <div className="col-lg" data-aos="fade-right" data-aos-delay="100">
                    <div className="card w-100" style={{ width: 'fit-content' }}>
                        <div className="card-body">
                            <img src="images/why_us/icon_24hrs.svg" className="img-responsive mb-3" width="32" alt=""/>
                            <h5 className="card-title">Layanan 24 Jam</h5>
                            <p className="card-text">Siap melayani kebutuhan Anda selama 24 jam nonstop. Kami juga tersedia di akhir minggu</p>
                        </div>
                    </div>
                </div>
                <div className="col-lg" data-aos="fade-right" data-aos-delay="150">
                    <div className="card w-100" style={{ width: 'fit-content' }}>
                        <div className="card-body">
                            <img src="images/why_us/icon_professional.svg" className="img-responsive mb-3" width="32" alt=""/>
                            <h5 className="card-title">Supir Professional</h5>
                            <p className="card-text">Sopir yang profesional, berpengalaman, jujur, ramah dan selalu tepat waktu</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>

        {/* TESTIMONIAL */}
        <div id="testi" style={{ paddingTop: '15px' }}></div>
        <div className="container-fluid container-sect" style={{ marginBottom: '40px' }}>
        <h1 id="testi" className="text-center" style={{ color: 'black' }}><strong>Testimonial</strong></h1>
        <p className="text-center" style={{ color: 'black' }}>Berbagai review positif dari para pelanggan kami</p>
        </div>
            <div className="carousel"
            data-flickity='{ "wrapAround": true }'>
            <div className="carousel-cell">
            <div className="row">
                <div className="col-lg-2">
                <img src="images/testi/radian.jpg" width="100" className="img-responsive rounded-circle" alt="" style={{ position:'relative', left:'10%' }}/>
                </div>
                <div className="col-10" style={{ position: 'relative' }}>
                    <div className="d-flex">
                    <img src="images/testi/Star_5.svg" height="16" alt=""/>
                    <img src="images/testi/Star_5.svg" height="16" alt=""/>
                    <img src="images/testi/Star_5.svg" height="16" alt=""/>
                    <img src="images/testi/Star_5.svg" height="16" alt=""/>
                    <img src="images/testi/Star_5.svg" height="16" alt=""/>
                    </div>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Possimus, in reiciendis veritatis repellat ad beatae sequi ipsum dolores quas reprehenderit, tempore soluta commodi vel facere doloribus libero rerum quia natus distinctio maxime voluptatibus, magni sapiente quos! Nisi, nesciunt placeat architecto, corporis cumque blanditiis voluptate culpa neque velit hic deserunt quae!</p>
                    <h1>Radian Rasyid, Banjar</h1>
                </div>
            </div>
            </div>
            <div className="carousel-cell">
            <div className="row">
                <div className="col-lg-2">
                <img src="images/testi/radian.jpg" width="100" className="img-responsive rounded-circle" alt="" style={{ position:'relative', left:'10%' }}/>
                </div>
                <div className="col-10" style={{ position: 'relative' }}>
                <div className="d-flex">
                    <img src="images/testi/Star_5.svg" height="16" alt=""/>
                    <img src="images/testi/Star_5.svg" height="16" alt=""/>
                    <img src="images/testi/Star_5.svg" height="16" alt=""/>
                    <img src="images/testi/Star_5.svg" height="16" alt=""/>
                    <img src="images/testi/Star_5.svg" height="16" alt=""/>
                </div>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Possimus, in reiciendis veritatis repellat ad beatae sequi ipsum dolores quas reprehenderit, tempore soluta commodi vel facere doloribus libero rerum quia natus distinctio maxime voluptatibus, magni sapiente quos! Nisi, nesciunt placeat architecto, corporis cumque blanditiis voluptate culpa neque velit hic deserunt quae!</p>
                <h1>Radian Rasyid, Batam</h1>
                </div>
            </div>
            </div>
            <div className="carousel-cell">
            <div className="row">
                <div className="col-lg-2">
                <img src="images/testi/radian.jpg" width="100" className="img-responsive rounded-circle" alt="" style={{ position:'relative', left:'10%' }}/>
                </div>
                <div className="col-10" style={{ position: 'relative' }}>
                <div className="d-flex">
                    <img src="images/testi/Star_5.svg" height="16" alt=""/>
                    <img src="images/testi/Star_5.svg" height="16" alt=""/>
                    <img src="images/testi/Star_5.svg" height="16" alt=""/>
                    <img src="images/testi/Star_5.svg" height="16" alt=""/>
                    <img src="images/testi/Star_5.svg" height="16" alt=""/>
                </div>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Possimus, in reiciendis veritatis repellat ad beatae sequi ipsum dolores quas reprehenderit, tempore soluta commodi vel facere doloribus libero rerum quia natus distinctio maxime voluptatibus, magni sapiente quos! Nisi, nesciunt placeat architecto, corporis cumque blanditiis voluptate culpa neque velit hic deserunt quae!</p>
                <h1>Radian Rasyid, Bromo</h1>
                </div>
            </div>
        </div>
        </div>

        {/* SEWA MOBIL */}
        <div id="sewa" style={{ marginTop: '100px' }}></div>
        <div style={{ marginTop: '200px' }}></div>
        <div data-aos="flip-left" data-aos-duration="1000" className="container container-mulai-sewa text-center">
        <h1 className="sewa-main">Sewa Mobil di Batam Sekarang</h1>
        <div className="container">
            <p className="sewa-desc" style={{ marginBottom: '52px' }}>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
            </p>
        </div>
        <div className="container ">
            <a id="sewabtn" className="mulai-sewa text-light" href='/sewa'>Mulai Sewa Mobil</a>
        </div>
        </div>

        {/* FAQ */}
        <div id="faq" style={{ marginBottom: '150px' }}></div>
        <div className="container-fluid container-sect" style={{ marginBottom: '100px' }}>
        <div className="row" style={{ width: '100%' }}>
            <div data-aos="fade-right" data-aos-duation="1000" className="col">
                <div className="container faq">
                <h2><strong>Frequently Asked Question</strong></h2>
                <p>Pertanyaan yang biasanya ditanyakan penyewa nih!</p>
                </div>
            </div>
            <div className="col col-accordion" style={{ width: '100%' }}>
                <div className="accordion container" id="accordionPanelsStayOpenExample">
                <div className="accordion-item" data-aos="fade-up" data-aos-delay="0" style={{ width: '100%' }}>
                    <h2 className="accordion-header" id="panelsStayOpen-headingOne">
                    <button className="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                        <i className="fa-solid fa-circle-question mr-3"></i> Apa saja syarat yang dibutuhkan?
                    </button>
                    </h2>
                    <div id="collapseOne" className="accordion-collapse collapse" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                    <div className="accordion-body">
                        <p>Kamu hanya harus menampilkan SIM dan KTP yang masih berlaku.</p>
                    </div>
                    </div>
                </div>
                <div id="freq" className="accordion-item" data-aos="fade-up" data-aos-delay="50" style={{ width: '100%' }}>
                    <h2 className="accordion-header" id="panelsStayOpen-headingTwo">
                    <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                        <i className="fa-solid fa-circle-question mr-3"></i> Berapa hari minimal sewa mobil lepas kunci?
                    </button>
                    </h2>
                    <div id="collapseTwo" className="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                    <div className="accordion-body">
                        <p>Minimal untuk mobil lepas kunci di daerah selain Batam adalah 1 hari penyewaan, di daerah Batam minimal 2 hari penyewaan</p>
                    </div>
                    </div>
                </div>
                <div className="accordion-item" data-aos="fade-up" data-aos-delay="100" style={{ width: '100%' }}>
                    <h2 className="accordion-header" id="panelsStayOpen-headingThree">
                    <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                        <i className="fa-solid fa-circle-question mr-3"></i> Berapa hari sebelumnya sabaiknya booking sewa mobil?
                    </button>
                    </h2>
                    <div id="collapseThree" className="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionExample">
                    <div className="accordion-body">
                        <p>Sebaiknya kamu melakukan booking maksimal 1 hari tanggal penyewaan.</p>
                    </div>
                    </div>
                </div>
                <div className="accordion-item" data-aos="fade-up" data-aos-delay="150" style={{ width: '100%' }}>
                    <h2 className="accordion-header" id="panelsStayOpen-headingFour">
                    <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                        <i className="fa-solid fa-circle-question mr-3"></i> Apakah Ada biaya antar-jemput?
                    </button>
                    </h2>
                    <div id="collapseFour" className="accordion-collapse collapse" aria-labelledby="headingFour" data-bs-parent="#accordionExample">
                    <div className="accordion-body">
                        <p>Antar jemput hanya berlaku untuk penyewa yang berdomisili di luar batam</p>
                    </div>
                    </div>
                </div>
                <div className="accordion-item" data-aos="fade-up" data-aos-delay="200" style={{ width: '100%' }}>
                    <h2 className="accordion-header" id="panelsStayOpen-headingFive">
                    <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                        <i className="fa-solid fa-circle-question mr-3"></i> Bagaimana jika terjadi kecelakaan?
                    </button>
                    </h2>
                    <div id="collapseFive" className="accordion-collapse collapse" aria-labelledby="headingFive" data-bs-parent="#accordionExample">
                    <div className="accordion-body">
                        <p>Jika terjadi kecelakaan yang terjadi karena human error maka penyewa akan menanggung, namun jika karena mobil yang error maka kami akan menanggung biaya anda.</p>
                    </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
        </div>
    </>
  )
}
