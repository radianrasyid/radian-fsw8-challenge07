import React from 'react'
import Footer from '../Components/Footer'
import HomeBody from '../Components/HomeBody'
import Navbar from '../Components/Navbar'

export default function Home() {
  return (
    <>
        <Navbar/>
        <HomeBody/>
        <Footer/>
    </>
  )
}
